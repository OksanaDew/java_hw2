package ua.danit.fs11;

import java.util.Random;
import java.util.Scanner;

public class AreaShooting {
    public static void main(String[] args) {
        for (int i = 0; i < 6; i++) {
            System.out.print(i + " | ");
        }
        System.out.println();

        char[][] fields = new char[5][5];

        for (int i = 0; i < 5; i++) {
            System.out.print(i + 1 + " | ");
            for (int j = 0; j < 5; j++) {
                fields[i][j]= '-';
                System.out.print(fields[i][j] + " | ");
            }
            System.out.println();
        }

        Random random = new Random();
        int line = random.nextInt(5);
        int coll = random.nextInt(5);

        Scanner scanner = new Scanner(System.in);

        System.out.println("All set. Get ready to rumble!");

        int userLine = 6;
        int userColl = 6;

        while(userLine - 1 != line || userColl - 1 != coll){

            System.out.println("Please enter a number of line");
            String userStrLine = scanner.next();

            try{
                userLine = Integer.parseInt(userStrLine);
            }catch (Exception e){
                System.out.println("Not a number! Please enter a number");
            }

            System.out.println("Please enter a number of coll");
            String userStrColl = scanner.next();

            try{
                userColl = Integer.parseInt(userStrColl);
            }catch (Exception e){
                System.out.println("Not a number! Please enter a number");
            }

            if (userLine - 1 == line || userColl - 1 == coll){
                fields[userLine - 1][userColl - 1] = 'x';
                System.out.println("You have won!");
            } else {
                fields[userLine - 1][userColl - 1] = '*';
            }

            for (int i = 0; i < 6; i++) {
                System.out.print(i + " | ");
            }
            System.out.println();

            for (int i = 0; i < 5; i++) {
                System.out.print(i + 1 + " | ");

                for (int j = 0; j < 5; j++) {
                    System.out.print(fields[i][j] + " | ");
                }
                System.out.println();
            }
        }
    }
}
